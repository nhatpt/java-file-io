/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileIO_SV;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Hades
 */

public class IOFile {

    //Đọc, ghi sinh viên:
    public void ghiSV(ArrayList<SinhVienClass> list , File file) {
        try (PrintWriter pw = new PrintWriter(file)) {
            for (SinhVienClass s : list) {
                pw.println(s.getMSSV());
                pw.println(s.getTen());
                pw.println(s.getNgaySinh());
            }
            pw.close();
        } catch (Exception e) {
            System.out.println("Got an Write-Student exception!");
        }
    }

    public ArrayList docSV(File file) throws FileNotFoundException {
        ArrayList<SinhVienClass> list = new ArrayList<>();
            try (Scanner sc = new Scanner(file)) {
                while (sc.hasNext()) {
                    SinhVienClass sv = new SinhVienClass();
                    
                    sv.setMSSV(sc.nextLine());
                    sv.setTen(sc.nextLine());
                    sv.setNgaySinh(sc.nextLine());
                    
                    list.add(sv);
                }
                sc.close();
            }
         catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
