/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package FileIO_SV;

import java.io.Serializable;

/**
 *
 * @author Hades
 */
public class SinhVienClass implements Serializable, InterfaceToObj{
    String MSSV;
    String Ten;
    String ngaySinh;

    public SinhVienClass() {
    }

    public SinhVienClass(String MSSV, String Ten, String ngaySinh) {
        this.MSSV = MSSV;
        this.Ten = Ten;
        this.ngaySinh = ngaySinh;
    }

    public void setMSSV(String MSSV) {
        this.MSSV = MSSV;
    }

    public void setNgaySinh(String ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public void setTen(String Ten) {
        this.Ten = Ten;
    }

    public String getMSSV() {
        return MSSV;
    }

    public String getNgaySinh() {
        return ngaySinh;
    }

    public String getTen() {
        return Ten;
    }

    @Override
    public Object[] toObject() {
       return new Object[]{ this.getMSSV(), this.getTen(), this.getNgaySinh() };
    }
}
